# README #

This is the repository for lab02 SRE. Implementing the triggers in trigger.exe in Java.

## Contents ##

* src/Main: Main method to call the triggers
* src/org/Triggers: The methods for the 6 triggers.

### Triggers ###

* Trigger 1: "I l0v3 C0mput3r $ci3nc3." 
    * Hardcoded
* Trigger 2: "pass.txt"
    * Create an 8 character filename
* Trigger 3: "6,1,3,7,15,31"
    * F(0) = (0*2)+1      //Base Case
    * F(n) = (F(n-1)*2)+1 
* Trigger 4
    * Checks the following values in array: 4->1, 6->8, 8->20, 9->92
* Trigger 5
* Trigger 6


### Who do I talk to? ###

* Author: Erick Garcia <egarcia87@miners.utep.edu>
* Other: Lorna Bustillos & Angelica Martinez