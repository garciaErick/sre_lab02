import org.Triggers;
import java.util.Scanner;

/**
 * @desc Main class. Interacts with /org/Triggers/
 * @author Erick Garcia <egarcia87@miners.utep.edu>
 * @author Angelica Martinez
 * @author Lorna Bustillos
 */
public class Main {
  public static final void main(String[] args) {
    Scanner sc         = new Scanner(System.in);
    String[] passwords = new String[4];
    printMessage(0);
    passwords[0] = sc.nextLine();
    if (Triggers.trigger1(passwords[0])) { //Trigger 1
      printMessage(1);
      passwords[1] = sc.nextLine();
    }
    else
      error();
    if (Triggers.trigger2(passwords[1])) { //Trigger 2
      printMessage(2);
      passwords[2] = sc.nextLine();
    }
    else
      error();
    if (Triggers.trigger3(passwords[2])) { //Trigger 3
      printMessage(3);
      passwords[3] = sc.nextLine();
    }
    else
      error();
    if (Triggers.trigger4(passwords[3])) { //Trigger 4
      printMessage(4);
    }
    else
      error();
    printMessage(5);                       //Success 
    sc.close();
  }

  /**
   * @desc  Output for each of the triggers
   * @param trigger_no - Which trigger is it calling from
   */
  public static void printMessage(int trigger_no){
    String[] messages = {
      "Please help! What is the first passphrase",
      "Trigger 1 diffused. Good! Now what about the 2nd",
      "Trigger 2 diffused. Excellent! Next one?", 
      "Trigger 3 diffused. The bad Scientist is getting nervous! Next?",
      "Trigger 4 diffused. You are doing great!",
      "Congratulations, you have saved the planet. Good Job !!!",
      "You have failed to save the human race, kindly switch majors."
    };
    System.out.println(messages[trigger_no]);
  }

  /**
   * @desc  Output for invalid trigger passwords
   */
  public static void error(){
    printMessage(6);
    System.exit(0);
  }
}
