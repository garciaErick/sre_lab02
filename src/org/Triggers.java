package org;
import java.io.File;
import java.util.Scanner;

/**
 * @desc Class that implements all of the trigger methods
 * @author Erick Garcia <egarcia87@miners.utep.edu>
 * @author Angelica Martinez
 * @author Lorna Bustillos
 */
public class Triggers{

  /**
   * @desc   Hardcoded value "I l0v3 C0mput3r $ci3nc3.".
   * @param  u_pass    - user input
   * @return isDifused - if the user put the correct password
   */
  public static boolean trigger1(String u_pass){
    String pass        = "I l0v3 C0mput3r $ci3nc3.";
    boolean isDiffused = (pass.equals(u_pass)) ? true : false;
    return isDiffused;
  }

  /**
   * @desc   The password is creating an 8 character file name 
   * and use that as input
   * @param  file_name  - user input, the file name
   * @return check_file - Checks if the file is there or not 
   */
  public static boolean trigger2(String file_name){
    boolean check_file = new File("files", file_name).exists();
    return check_file;
  }

  /**
   * @desc   The password uses the following function:
   * Precondition: input hardcoded value 6
   * F(0) = user_input
   * F(n) = (F(n -1) * 2) + 1
   * E.g 1,3,7,15,31
   * @param  input      - user input, the file name
   * @return isDiffused - Checks if the file is there or not 
   */
  public static boolean trigger3(String input){
    Scanner sc   = new Scanner(System.in);
    int loop     = Integer.parseInt(input);
    int[] u_pass = new int[loop - 1];
    int[] pass   = new int[loop - 1];
    boolean isDiffused = false;
    if (loop == 6) {
      u_pass[0] = sc.nextInt(); //There is no exact value, it -
      pass[0]   = u_pass[0];    //  depends on user input
      for (int i = 1; i < loop - 1; i++) {
        u_pass[i] = sc.nextInt();
        pass[i]   = ( pass[i-1] * 2 ) + 1; //F(n)
      } 
      // sc.close();
      for (int i = 0; i < pass.length; i++) {
        if(u_pass[i] != pass[i])
          break;
        if(i == pass.length-1) //If done through the array means -
          isDiffused = true;   //  it was the correct combination
      }
    }
    return isDiffused;
  }

  /**
   * @desc   The password uses the following conditions:
   * Precondition: input hardcoded value 9
   * Orders the inputs from lowest to highest
   * Checks the following index-value combinations:
   *  4->1, 6->8, 8->20, 9->92
   * F(n) = (F(n -1) * 2) + 1
   * E.g 1,3,7,15,31
   * @param  input      - user input, the file name
   * @return isDiffused - Checks if the file is there or not 
   */
  public static boolean trigger4(String input){
    Scanner sc   = new Scanner(System.in);
    int loop     = Integer.parseInt(input);
    int[] u_pass = new int[loop - 1];
    boolean isDiffused = false;
    if (loop == 9) {
      for (int i = 0; i < loop - 1; i++) {
        u_pass[i] = sc.nextInt();
      } 
      // sc.close();
      java.util.Arrays.sort(u_pass); //Sorts array
      if (u_pass[3] == 1)
        isDiffused = true;
      else if (u_pass[5] == 8)
        isDiffused = true;
      else if (u_pass[7] == 20)
        isDiffused = true;
      else if (u_pass[8] == 92)
        isDiffused = true;
    }
    return isDiffused;
  }

  public static boolean trigger5(String file_name){
    return false;
  }
}
